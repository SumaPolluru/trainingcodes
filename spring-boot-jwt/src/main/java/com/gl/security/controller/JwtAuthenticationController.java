package com.gl.security.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gl.security.config.JwtTokenUtil;
import com.gl.security.model.JwtRequest;
import com.gl.security.model.JwtResponse;
import com.gl.security.model.UserDTO;
import com.gl.security.service.JwtUserDetailsService;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	/*
	 * @Autowired private AuthenticationManager authenticationManager;
	 * 
	 * @Autowired private JwtTokenUtil jwtTokenUtil;//which containing information
	 * about token //like expiry date
	 * 
	 * @Autowired private UserDetailsService jwtInMemoryUserDetailsService;
	 * 
	 * @RequestMapping(value = "/authenticate", method = RequestMethod.POST) public
	 * ResponseEntity<?> createAuthenticationToken (@RequestBody JwtRequest
	 * authenticationRequest) throws Exception {
	 * 
	 * authenticate(authenticationRequest.getUsername(),
	 * authenticationRequest.getPassword());
	 * 
	 * final UserDetails userDetails = jwtInMemoryUserDetailsService
	 * .loadUserByUsername(authenticationRequest.getUsername());
	 * 
	 * final String token = jwtTokenUtil.generateToken(userDetails);
	 * 
	 * return ResponseEntity.ok(new JwtResponse(token)); }
	 * 
	 * private void authenticate(String username, String password) throws Exception
	 * { Objects.requireNonNull(username); Objects.requireNonNull(password);
	 * 
	 * try { authenticationManager.authenticate( new
	 * UsernamePasswordAuthenticationToken(username, password)); } catch
	 * (DisabledException e) { //if username and password is correct only then it
	 * will generate token throw new Exception("USER_DISABLED", e); } catch
	 * (BadCredentialsException e) { throw new Exception("INVALID_CREDENTIALS", e);
	 * } }
	 */

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	// public ResponseEntity<?> createAuthenticationToken(
	// @RequestBody JwtRequest authenticationRequest) throws Exception {
	public ResponseEntity<?> createAuthenticationToken(
			@ModelAttribute("userobj") UserDTO authenticationRequest) throws Exception {
		System.out.println("username is :" + authenticationRequest.getUsername());
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);
		System.out.println("Token displaying.......");
		System.out.println("Token is " + token);
		return ResponseEntity.ok(new JwtResponse(token));
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> saveUser(@ModelAttribute("userobj") UserDTO user) throws Exception {
		return ResponseEntity.ok(userDetailsService.save(user));
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}

}
