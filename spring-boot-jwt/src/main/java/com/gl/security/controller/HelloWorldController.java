package com.gl.security.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.gl.security.model.UserDTO;

@RestController
@CrossOrigin()
public class HelloWorldController {

	@RequestMapping({ "/hello" })
	public String hello() {
		return "Hello World";
	}
	@RequestMapping("/UserDetails")
	public ModelAndView insertForm() {
		return new ModelAndView("RegisterationForm","userobj",new UserDTO());
	}
	@RequestMapping("/login")
	public ModelAndView loginForm() {
		return new ModelAndView("Login","userobj",new UserDTO());
	}

}
