package com.gl.security.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gl.security.model.DAOUser;

@Repository
public interface UserDao extends JpaRepository<DAOUser,Integer> {

	DAOUser findByUsername(String username);
}
