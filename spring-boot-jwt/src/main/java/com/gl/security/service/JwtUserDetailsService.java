package com.gl.security.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.gl.security.dao.UserDao;
import com.gl.security.model.DAOUser;
import com.gl.security.model.UserDTO;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	/*
	 * @Override public UserDetails loadUserByUsername(String username) throws
	 * UsernameNotFoundException { if ("Dimple".equals(username)) { return new
	 * User("Dimple",
	 * "$2a$12$CLAyRG/WB46wN4j7GJw/ceoH52qpmd3CrTH9SUgDKBqUElzfqq5dW", new
	 * ArrayList<>()); } else { throw new
	 * UsernameNotFoundException("User not found with username: " + username); } }
	 */

	@Autowired
	private UserDao userDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		DAOUser user = userDao.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}

	// http://localhost:8080/register
	public DAOUser save(UserDTO user) {
		System.out.println("Coming here");
		DAOUser newUser = new DAOUser();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		return userDao.save(newUser);
	}
}