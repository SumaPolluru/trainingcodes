<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Web Application Using Spring Boot</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}

td>form>label {
	border: 2px solid black;
	box-sizing: border-box;
	display: inline-block;
	padding: 12px 25px;
}
</style>

<body>
	<h1>Register</h1>
	<form:form method="POST" action="register" modelAttribute="userobj">
		<table>
			<tr>
				<td><form:label path="username"><font color=Black size=5>User name</font></form:label></td>
				<td><form:input path="username" /></td>
			</tr>
			<tr>
				<td><form:label path="password"><font color=Black size=5>Password</font></form:label></td>
				<td><form:input path="password" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Register" /></td>
			</tr>
		</table>
		<table>
		<tr>
			<td><a href="login">Login</a></td>
		</tr>
	</table>
		
	</form:form>
</body>

</html>