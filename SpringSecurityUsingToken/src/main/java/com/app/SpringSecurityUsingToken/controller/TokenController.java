package com.app.SpringSecurityUsingToken.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class TokenController {
	
	@Autowired
	RestTemplate restTemplate;

//its a class which can be used to call any external web service
	@RequestMapping(value = "/")
	public String getUserBooksList() {
		HttpHeaders headers = new HttpHeaders();
		headers.setBearerAuth(
				"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdW1hQGdtYWlsLmNvbSIsImV4cCI6MTY2ODE4MjE1OCwiaWF0IjoxNjY4MTY0MTU4fQ.wfO5Z39EhNg22NXpfBF8V4ApVivQ1NnGBJy-PG350K8TjKYc4wfqmFDcr5LUUSsjBz9H0gPbWDf-5wtJg3klWA");
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		return restTemplate.exchange("http://localhost:8090/userData", HttpMethod.GET, entity, String.class).getBody();
	}
}