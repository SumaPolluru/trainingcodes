package myservletapp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import connect.DataConnect;
/**
 * Servlet implementation class InsertServlet
 */
@WebServlet("/InsertServlet")
public class InsertServlet extends HttpServlet {
	public void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException
	,IOException
	{
		
		int taskId=Integer.parseInt(req.getParameter("taskId"));
		String tasktitle=req.getParameter("taskTitle");
		int duration=Integer.parseInt(req.getParameter("duration"));
		String user=req.getParameter("user");
		Connection con=DataConnect.getConnection();
		try
		{
		PreparedStatement stat=con.prepareStatement("insert into task values(?,?,?,?)");
		stat.setInt(1, taskId);
		stat.setString(2, tasktitle);
		stat.setInt(3, duration);
		stat.setString(4, user);
		int result=stat.executeUpdate();
		if(result>0)
		{
			System.out.println("inserted successfully");
		}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
				
	}
}
