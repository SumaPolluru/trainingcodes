package com.gl.app.SpringCore.dao;

import com.gl.app.SpringCore.pojo.Product;

public interface ProductDAO {
	public void insert(Product p);

	public Product getProduct();

}
