
package com.gl.app.SpringCore.annotations;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
		public static void main(String[] args) {
			/*ApplicationContext cont=new AnnotationConfigApplicationContext(EmployeeConfig.class);
			Employee eobj=(Employee)cont.getBean("empobj1");
			System.out.println("First name is "+eobj.getEmpName());*/
					ApplicationContext cont=new AnnotationConfigApplicationContext(EmployeeConfig.class);
					Employee eobj=(Employee)cont.getBean("empobj1");
					System.out.println("First name is "+eobj.getEmpName());
					Department d=(Department) cont.getBean("dept");
					System.out.println("Department name is "+d.getDeptname());
					System.out.println("Employees in department");
					java.util.List<Employee> list1=d.getEmplist();
					for(Employee e:list1)
						
					{
						System.out.println("Name is "+e.getEmpName());
					}
		
}

}
