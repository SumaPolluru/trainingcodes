package com.gl.app.SpringCore.dao;

import org.springframework.jdbc.core.JdbcTemplate;

import com.gl.app.SpringCore.pojo.Product;

public class ProductDAOImpl implements ProductDAO {
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	private JdbcTemplate jdbcTemplate;

	@Override
	public void insert(Product p) {
		String query = "insert into Product values(?,?,?)";
		jdbcTemplate.update(query, p.getProductId(), p.getProductName(), p.getPrice());
		System.out.println("Inserted succesfully");

	}

	@Override
	public Product getProduct() {
		// TODO Auto-generated method stub
		return null;
	}

}
