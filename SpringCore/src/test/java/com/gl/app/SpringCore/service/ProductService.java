package com.gl.app.SpringCore.service;

import java.util.Scanner;

import javax.print.PrintService;

import com.gl.app.SpringCore.dao.ProductDAOImpl;
import com.gl.app.SpringCore.pojo.Product;

public class ProductService {
	private Scanner sc;
	private Product p;
	private ProductDAOImpl pdaoimpl;

	public void setPdaoimpl(ProductDAOImpl pdaoimpl) {
		this.pdaoimpl = pdaoimpl;
	}

	public ProductService() {
		sc = new Scanner(System.in);
	}

	public void insertData() {
		p = new Product();
		System.out.println("Enter product Id ");
		p.setProductId(sc.nextInt());
		System.out.println("Enter Product name ");
		p.setProductName(sc.next());
		System.out.println("Enter price ");
		p.setPrice(sc.nextDouble());
		pdaoimpl.insert(p);
	}
}