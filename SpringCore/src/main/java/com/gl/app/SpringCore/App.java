package com.gl.app.SpringCore;

import java.util.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	/*ApplicationContext appcont=new ClassPathXmlApplicationContext("Beans.xml");
    	Employee emp=(Employee)appcont.getBean("empobj");
    	System.out.println("Employee Id is "+emp.getEmpId());
    	System.out.println("Employee name is "+emp.getEmpName());
    	System.out.println("Employee salry is "+emp.getSalary());
    	Employee emp1=(Employee)appcont.getBean("empobj1");
    	System.out.println("Employee Id is "+emp1.getEmpId());
    	System.out.println("Employee name is "+emp1.getEmpName());
    	System.out.println("Employee salary is "+emp1.getSalary());*/
    	
    	
    	ApplicationContext appcont=new ClassPathXmlApplicationContext("Beans.xml");
    	Department dept=(Department)appcont.getBean("deptobj");
    	System.out.println("Department code is "+dept.getDeptcode());
    	System.out.println("Department name is "+dept.getDeptname());
    	List<Employee> elist=dept.getEmplist();
    	for(Employee e:elist)
    	{
    		System.out.println("Employee name is "+e.getEmpName());
    	}
    	
    }
}
