package com.example.SearchQuery;

import com.example.SearchQuery.Query;
import com.example.SearchQuery.QueryResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

public class SearchController {
	private final ClientSearch ClientSearch;

    public SearchController(ClientSearch ClientSearch) {
        this.ClientSearch = ClientSearch;
    }
}