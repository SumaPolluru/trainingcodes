package com.example.SearchQuery;
import com.example.SearchQuery.Query;
import com.example.SearchQuery.QueryResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface ClientSearch  {
	@GetMapping("/api/as/v1/engines/${appsearch.engine}/search")
    QueryResponse search(@RequestBody Query query);
	

}
