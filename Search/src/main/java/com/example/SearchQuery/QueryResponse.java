package com.example.SearchQuery;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;
import java.util.Map;


public class QueryResponse {

	    private List<Result> results = Collections.emptyList();

	    public List<Result> getResults() {
	        return results;
	    }

	    public void setResults(List<Result> results) {
	        this.results = results;
	    }

	    public static class Result {

	        private int QuestionNo;

	        private String QuestionAnswer;


	       
	        public int getQuestionNo() {
				return QuestionNo;
			}

			public void setQuestionNo(int questionNo) {
				QuestionNo = questionNo;
			}

			public String getQuestionAnswer() {
				return QuestionAnswer;
			}

			public void setQuestionAnswer(String questionAnswer) {
				QuestionAnswer = questionAnswer;
			}

			// this is a ugly work around to set a field from a nested object
	        // I had hoped one can a specify a nested path in the property definition, but apparently
	        // this is the way to go...

	        @JsonProperty("QuestionAnswer")
	        private void unpackHostFromNestedObject(Map<String, String> map) {
	            this.QuestionAnswer = map.get("raw");
	        }
	    }
	}