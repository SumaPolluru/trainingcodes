package com.example.SpringInheritance.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.SpringInheritance.model.Employee;

public interface EmployeeRepositories extends JpaRepository<Employee, Integer> {

}
