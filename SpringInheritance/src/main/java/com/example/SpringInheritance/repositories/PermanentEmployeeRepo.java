package com.example.SpringInheritance.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.SpringInheritance.model.PEmployee;

public interface PermanentEmployeeRepo extends JpaRepository<PEmployee, Integer> {

}
