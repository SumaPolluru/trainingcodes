package com.example.SpringInheritance.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "TEmployeeConcrete")
@AttributeOverrides
({
	@AttributeOverride(name = "empId", column = @Column(name = "EmployeeId")),
	@AttributeOverride(name = "empName", column = @Column(name = "EmployeeName")) 
})
/*
 * @Entity
 * 
 * @Table(name="TEmployeeSubClass")
 * 
 * @Inheritance(strategy=InheritanceType.JOINED)
 */
/*
 * @Entity
 * 
 * @DiscriminatorValue("TempEmployee")
 */
public class TempEmployee extends Employee {
	private double contAmount;
	private int duration;

	public double getContAmount() {
		return contAmount;
	}

	public void setContAmount(double contAmount) {
		this.contAmount = contAmount;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}
