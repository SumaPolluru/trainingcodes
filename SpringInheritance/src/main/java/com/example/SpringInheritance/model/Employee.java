package com.example.SpringInheritance.model;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="EmployeeConcrete")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
/*@Entity
@Table(name="EmployeeSubClass")
@Inheritance(strategy=InheritanceType.JOINED)*/
/*@Table(name="EmployeeInheritance")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="EmpType",discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue("EmployeeData")*/
public class Employee {
    @Id
	private int empId;
	private String empName;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

}
