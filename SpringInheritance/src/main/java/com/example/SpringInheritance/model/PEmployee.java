package com.example.SpringInheritance.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name="PEmployeeConcrete")
@AttributeOverrides(
		{
		@AttributeOverride
		(name="empId",column=@Column(name="EmployeeId")),
		@AttributeOverride
		(name="empName",column=@Column(name="EmployeeName"))
		})

/*@Entity
@Table(name="PEmployeeSubClass")
@Inheritance(strategy=InheritanceType.JOINED)*/
@PrimaryKeyJoinColumn(name="empId")
/*@Entity
@DiscriminatorValue("PremEmployee")     these for Inheritance*/
public class PEmployee extends Employee {
	private double salary;
	private double bonus;

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

}
