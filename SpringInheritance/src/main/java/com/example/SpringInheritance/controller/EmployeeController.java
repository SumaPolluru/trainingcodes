package com.example.SpringInheritance.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.SpringInheritance.model.Employee;
import com.example.SpringInheritance.model.PEmployee;
import com.example.SpringInheritance.repositories.EmployeeRepositories;
import com.example.SpringInheritance.repositories.PermanentEmployeeRepo;

@Controller
public class EmployeeController {
	@Autowired
	EmployeeRepositories emprep;
	@Autowired
	PermanentEmployeeRepo pemprepo;
	
	@RequestMapping("/EmployeeData")
	public String getData()
	{
		Employee emp=new Employee();
		emp.setEmpId(1);
		emp.setEmpName("Anaita");
		emprep.save(emp);
		return "success";
	}
	@RequestMapping("/PEmpData")
	public String storePEmpData()
	{
		PEmployee pemp=new PEmployee();
		pemp.setEmpId(2);
		pemp.setSalary(90000);
		pemp.setEmpName("Joseph");
		pemp.setBonus(900);
		pemprepo.save(pemp);
		return "success";
		
	}

}