package com.java.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.java.beans.UserContacts;
import com.java.dao.UserContactDao;

@Controller
public class UserContactController {
	@Autowired
	UserContactDao dao;
	
	@RequestMapping("/empform")
	public String showform(Model m) {
		m.addAttribute("command", new UserContacts());
		return "empform";
	}

	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("emp") UserContacts emp) {
		dao.save(emp);
		return "redirect:/viewemp";
	}

	
	@RequestMapping("/viewemp")
	public String viewemp(Model m) {
		List<UserContacts> list = dao.getContacts();
		m.addAttribute("list", list);
		return "viewemp";
	}

	
	@RequestMapping(value = "/editemp/{userId}")
	public String edit(@PathVariable int userId, Model m) {
		UserContacts emp = dao.getEmpById(userId);
		m.addAttribute("command", emp);
		return "empeditform";
	}

	
	@RequestMapping(value = "/editsave", method = RequestMethod.POST)
	public String editsave(@ModelAttribute("emp") UserContacts emp) {
		dao.update(emp);
		return "redirect:/viewemp";
	}

	
	@RequestMapping(value = "/deleteemp/{userId}", method = RequestMethod.GET)
	public String delete(@PathVariable int userId) {
		dao.delete(userId);
		return "redirect:/viewemp";
	}
}