    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<h1>Contacts List</h1>  
<table border="2" width="70%" cellpadding="2">  
<tr><th>UserId</th><th>ContactFirstName</th><th>ContactLastName</th><th>Email</th><th>MobileNo</th><th>Date</th><th>Update</th><th>	Delete</th></tr>  
   <c:forEach var="UserContacts" items="${list}">   
   <tr>  
   <td>${UserContacts.userId}</td>  
   <td>${UserContacts.contactFirstName}</td>  
   <td>${UserContacts.contactLastName}</td>  
   <td>${UserContacts.email}</td>
   <td>${UserContacts.mobileno}</td>
   <td>${UserContacts.date}</td>  
   <td><a href="editemp/${UserContacts.userId}">Edit</a></td>
    <td><a href="deleteemp/${UserContacts.userId}">Delete</a></td>
   </tr>  
   </c:forEach>  
   </table>  
   <br/>  
    <a href="empform">Add New Contact</a>